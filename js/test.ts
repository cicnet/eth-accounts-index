import { AccountRegistry } from './src/registry';

const fs = require('fs');

const Web3 = require('web3');

const w3 = new Web3('http://localhost:8545');

const d = fs.readFileSync('../solidity/registry.abi.json');
const abi = JSON.parse(d);

const address = process.argv[2];

const r = new AccountRegistry(w3, abi, address);

async function main() {
	console.log(await r.count());

	console.log(await r.have('0xe3C4db5947409Aff0FF8D643047EA41515cA4B8e'));
	console.log(await r.have('0x34C85E47f45b0bea09F37c83e2fb02ECBC3a395d'));

	console.log(await r.last(3));
}

main();
